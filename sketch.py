from typing import Optional

import pickle

import dns
import dns.message
import dns.name
import dns.query
import dns.rdatatype
from dns.rdata import Rdata

ROOT = "2001:500:200::b"

ANSWER = 0
DELEGATION = 1
GLUE = 2

COLORS = {ANSWER: 'red',
          DELEGATION: 'black',
          GLUE: 'blue'}


class TruncatedError(Exception):
    def __init__(self, address, domain, rdtype):
        self.message = f"TC bit set in answer to {domain} {rdtype} from {address}"

    def __str__(self):
        return self.message


def ask(address: str, domain: dns.name.Name, rdtype: int, cache) -> Optional[dns.message.Message]:
    if (address, domain, rdtype) in cache:
        return cache[(address, domain, rdtype)]

    query = dns.message.make_query(domain, rdtype)
    query.use_edns()
    try:
        answer = dns.query.udp(query, address, timeout=5, ignore_trailing=True)
        if answer.rcode() not in (dns.rcode.NOERROR, dns.rcode.NXDOMAIN):
            query.use_edns(edns=-1)
            answer = dns.query.udp(query, address, timeout=5, ignore_trailing=True)
        if answer.flags & dns.flags.TC:
            raise TruncatedError(address, domain, rdtype)
    except Exception as error:
        print("udp:", domain, rdtype, address, error)
        try:
            answer = dns.query.tcp(query, address, timeout=5)
        except Exception as error2:
            print("tcp:", domain, rdtype, address, error2)
            answer = None
    cache[(address, domain, rdtype)] = answer
    return answer


def add_to_dict(message: dns.message.Message, dic, zone):
    add_section_to_dict(message.answer, ANSWER, dic, zone)
    add_section_to_dict(message.authority, DELEGATION, dic, zone)
    add_section_to_dict(message.additional, GLUE, dic, zone)


def add_section_to_dict(section, edge_type, dic, zone):
    for rrset in section:
        key = (rrset.name, rrset.rdtype)
        for rdata in rrset:
            if key not in dic:
                dic[key] = rdata, {(zone, edge_type)}
            else:
                dic[key][1].add((zone, edge_type))


def get_address(domain: dns.name.Name, dic, cache) -> Optional[str]:
    if domain == dns.name.from_text("."):
        return ROOT
    else:
        address_from_dic = get_address_from_dic(domain, dic)
        if address_from_dic:
            address_response = address_from_dic[0]
        else:
            address_response = res_minimize(domain, dns.rdatatype.A, dic, cache)
        if address_response is None:
            address_response = res_minimize(domain, dns.rdatatype.AAAA, dic, cache)
        if address_response is None:
            return None
        return address_response.address


def res(domain: dns.name.Name, rdtype: int, first_server, first_zone, dic, cache) -> Optional[Rdata]:
    if (domain, rdtype) in dic:
        if dic[(domain, rdtype)] is None:
            return None
        return dic[(domain, rdtype)][0]
    if (domain, dns.rdatatype.CNAME) in dic:
        return res_minimize(dic[(domain, dns.rdatatype.CNAME)][0].target, rdtype, dic, cache)
    print(domain, rdtype)
    server_stack = [(first_server, first_zone)]
    while server_stack:
        server, zone = server_stack.pop()
        print(server, zone)
        address = get_address(server, dic, cache)
        if not address:
            continue
        answer = ask(address, domain, rdtype, cache)
        if not answer:
            continue
        add_to_dict(answer, dic, zone)
        if answer.answer:
            cname_rrset = answer.get_rrset(answer.answer, domain, 1, dns.rdatatype.CNAME)
            if cname_rrset:
                for cname in cname_rrset:
                    cname_res = res_minimize(cname.target, rdtype, dic, cache)
                return cname_res
        else:
            for rrset in answer.authority:
                if rrset.rdtype == dns.rdatatype.NS:
                    new_servers = sorted([(rdata.target, rrset.name) for rdata in rrset],
                                         key=lambda serv: get_address_from_dic(serv[0], dic) is not None)
                    for new_server in new_servers:
                        server_stack.append(new_server)
    if (domain,rdtype) in dic and dic[(domain, rdtype)] is not None:
        return dic[(domain, rdtype)][0]
    dic[(domain, rdtype)] = None
    return None


def res_minimize(domain, rdtype, dic, cache):
    if (domain, rdtype) in dic:
        if dic[(domain, rdtype)] is None:
            return None
        return dic[(domain, rdtype)][0]
    server, zone = (dns.name.from_text("."), dns.name.from_text("."))  # TODO
    for i in range(len(domain)-1):
        result = res(dns.name.Name(domain[-i-1:]), dns.rdatatype.NS, server, zone, dic, cache)
        if result is not None:
            server, zone = result.target, dns.name.Name(domain[-i-1:])
    return res(domain, rdtype, server, zone, dic, cache)


def get_address_from_dic(server, dic):
    return dic.get((server, dns.rdatatype.AAAA), dic.get((server, dns.rdatatype.A)))


def main(domain_str: str, rdtype: int, cache_file: str, use_cache=False):
    cache = {}
    if use_cache:
        cache = load_cache(cache_file)
    dic = {}
    domain = dns.name.from_text(domain_str)
    res_minimize(domain, rdtype, dic, cache)
    save_cache(cache_file, cache)
    return dic


def dot_input(path: str, dic, edges=(ANSWER, DELEGATION, GLUE), labels=True):
    with open(path, 'w+') as f:
        f.write('digraph G {\n')
        for (name, rdtype), value in dic.items():
            if value is None:
                print("not exist:", name, rdtype)
                continue
            zones = value[1]
            for zone, edge_type in zones:
                if edge_type in edges:
                    f.write(f'\t"{zone}" -> "{name}" [color={COLORS[edge_type]}')
                    if labels:
                        f.write(f', label={dns.rdatatype.to_text(rdtype)}')
                    f.write(']\n')
            if rdtype == dns.rdatatype.CNAME:
                f.write(f'\t"{name}" -> "{value[0].target}" [style=dotted, label=CNAME]')

        f.write('}\n')


def load_cache(path):
    with open(path, "rb") as f:
        cache = pickle.load(f)
    return cache


def save_cache(path, cache):
    with open(path, "wb") as f:
        pickle.dump(cache, f)


dic = main("www.bbc.co.uk.", 1, "graph_cache", use_cache=False)
dot_input("names2.gv", dic, (ANSWER, DELEGATION))
