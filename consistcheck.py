#!/usr/bin/python3
import sys
from typing import Dict, List, Tuple, Optional, Set
import pickle
import argparse

import dns.name
import dns.rdatatype
import dns.message

from catcher import QueryData, Query, Endpoint, NoAnswer
import matchpart


class Group:
    """
    A group of servers that return the same answer
    """
    def __init__(self, answer: dns.message.Message, address: str,
                 diff: Optional[Dict[str, matchpart.DataMismatch]]):
        self.answer = answer
        self.addresses = [address]
        self.diff = diff

    def __str__(self):
        return str(self.addresses)

    def add_address(self, address: str):
        self.addresses.append(address)

    def diff_info(self, color=False) -> str:
        original_color = '\033[94m'
        no_color = '\033[0m'

        if self.diff is None:
            if color:
                return f'refferal: {self.addresses}\n{original_color}{self.answer}{no_color}'
            return f'refferal: {self.addresses}\n{self.answer}'
        if 'none' in self.diff:
            return f"{self.addresses} didn't response"
        msg = f'{self.addresses} differ in fields:\n'
        for field, mismatch in self.diff.items():
            msg += f';{field}\n'
            if color:
                msg += f'{mismatch.diff.color_str()}'
            else:
                msg += f'{mismatch.diff}'

        return msg


def find_differences(expected: dns.message.Message, got: dns.message.Message, match: Set[str])\
        -> Dict[str, matchpart.DataMismatch]:
    differences = {}
    if isinstance(got, NoAnswer):
        if isinstance(expected, NoAnswer):
            return {}
        return {"none": matchpart.DataMismatch(expected, got)}
    if isinstance(expected, NoAnswer):
        return {"none": matchpart.DataMismatch(expected, got)}
    for field in match:
        try:
            matchpart.MATCH[field](expected, got)
        except matchpart.DataMismatch as mismatch:
            differences[field] = mismatch
    return differences


def check_query_consistency(query: Query, zone: dns.name.Name,
                            database: QueryData, match: Set[str]):
    groups = []
    for address in database.get_ns_addresses(zone):
        answer = database.answers[query, Endpoint(address)]
        new = True
        for group in groups:
            if not find_differences(group.answer, answer, match):
                group.add_address(address)
                new = False
                break
        if new:
            groups.append(Group(answer, address, None))
    return select_refferal(groups, match)


def msg_len(msg: dns.message.Message):
    return len(msg.question) + len(msg.answer) + len(msg.authority) + len(msg.additional)


def select_refferal(groups, match: Set[str]):
    if not groups:
        return groups
    refferal = groups[0]
    ref_index = 0
    if not isinstance(refferal.answer, NoAnswer):
        ref_len = msg_len(refferal.answer)
    else:
        ref_len = None
    for i, group in enumerate(groups[1:], start=1):
        if not isinstance(group.answer, NoAnswer):
            new_len = msg_len(group.answer)
            if ref_len is None or new_len < ref_len:
                refferal = group
                ref_len = new_len
                ref_index = i

    new_groups = [refferal]
    for i, group in enumerate(groups):
        if i != ref_index:
            group.diff = find_differences(refferal.answer, group.answer, match)
            new_groups.append(group)
    return new_groups


def check_consistency(database: QueryData,
                      selected_query: Query,
                      match: Set[str]):
    groups: Dict[Tuple[Query, dns.name.Name], List[Group]] = {}

    # only one selected query
    if selected_query:
        for zone, queries in database.queries.items():
            if selected_query in queries:
                groups[(selected_query, zone)] = check_query_consistency(selected_query, zone,
                                                                         database, match)
        return groups

    # all queries
    for zone, queries in database.queries.items():
        if zone in database.nssets:
            for query in queries:
                groups[(query, zone)] = check_query_consistency(query, zone, database, match)
    return groups


def print_query_info(query: Tuple[Query, dns.name.Name], groups, color=False):
    bold = '\033[1m'
    no_color = '\033[0m'

    if color:
        print(f"{bold}", end="")
    print(f"{query[0].name.to_text()} {dns.rdatatype.to_text(query[0].rdtype)}",
          f"in zone {query[1]}")
    if color:
        print(f"{no_color}", end="")
    for group in groups:
        print(group.diff_info(color))


def parseargs():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("data",
                           help="path to pickled domain data")
    argparser.add_argument("-q", "--query", nargs=2, metavar=("DOMAIN", "TYPE"),
                           help="print only given query")
    argparser.add_argument("-a", "--all", action="store_true",
                           help="print all queries (not only inconsistent)")
    argparser.add_argument("-c", "--color", action="store_true",
                           help="colored output")
    argparser.add_argument("--noadditional", action="store_true",
                           help="ignore inconsistencies in additional section")
    args = argparser.parse_args()
    if args.query:
        name = dns.name.from_text(args.query[0])
        try:
            rdtype = dns.rdatatype.from_text(args.query[1])
        except (dns.rdatatype.UnknownRdatatype, ValueError):
            print(f"unknown rdtype '{args.query[1]}', skipping", file=sys.stderr)
            sys.exit()
        args.query = Query(name, rdtype)
    return args


def main():
    args = parseargs()
    try:
        with open(args.data, "rb") as f:
            data: QueryData = pickle.load(f)
    except FileNotFoundError as e:
        print(e)
        return

    match = {"opcode", "qtype", "qname", "qcase", "subdomain", "flags", "rcode",
             "answer", "answertypes", "answerrrsigs", "authority"}

    if not args.noadditional:
        match.add("additional")

    groups = check_consistency(data, args.query, match)
    inc_count = 0
    for query in groups:
        if len(groups[query]) > 1 or args.query or args.all:
            if len(groups[query]) > 1:
                inc_count += 1
            print_query_info(query, groups[query], color=args.color)
            print("--------------------------------------------------")
    print(f"{inc_count} of {len(groups)} queries inconsistent")


if __name__ == '__main__':
    main()
