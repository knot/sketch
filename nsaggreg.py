import argparse
import sys

from catcher import Query, QueryData, get_filename
import pickle
import dns.name


def get_nsset(filename: str, all_ns: bool, path_ns: bool):
    try:
        with open(filename, "rb") as f:
            data: QueryData = pickle.load(f)
    except Exception as e:
        print(e, sys.stdout)
        return None
    name = data.original_query.name
    while name not in data.nssets or not data.nssets[name]:
        name = dns.name.Name(name[1:])
    addresses = set()
    if all_ns:
        for zone_name in data.nssets:
            addresses.update(data.get_ns_addresses(zone_name))
    elif path_ns:
        for zone_name in data.nssets:
            if zone_name.is_superdomain(name):
                addresses.update(data.get_ns_addresses(zone_name))
    addresses.update(data.get_ns_addresses(name))
    return frozenset(addresses)


def get_nssets_clusters(queries_file: str, data_path, all_ns, path_ns):
    nssets = {}
    with open(queries_file) as queries:
        for line in queries:
            spl = line.split()
            if not spl:
                continue
            if len(spl) != 2:
                print(f"invalid length of line '{line}', skipping", file=sys.stderr)
                continue

            name = dns.name.from_text(spl[0])
            try:
                rdtype = dns.rdatatype.from_text(spl[1])
            except (dns.rdatatype.UnknownRdatatype, ValueError):
                print(f"unknown rdtype '{spl[1]}', skipping", file=sys.stderr)
                continue
            query = Query(name, rdtype)
            filename = get_filename(query, data_path)
            nsset = get_nsset(filename, all_ns, path_ns)
            if nssets is None:
                print(f"could not load data of {spl[0]} {spl[1]}, skipping", file=sys.stderr)
                continue
            if nsset in nssets:
                nssets[nsset].add(query)
            else:
                nssets[nsset] = {query}
    return nssets


def write_output(nssets, out):
    for nsset in nssets:
        out.write(f"{set(nsset[0])};")
        out.write(f"{len(nsset[1])};")
        for query in nsset[1]:
            out.write(f"{query.name.to_text()} {dns.rdatatype.to_text(query.rdtype)};")
        out.write("\n")


def create_report(args):
    nssets = get_nssets_clusters(args.queries, args.data, args.all, args.path)
    if None in nssets:
        nssets.pop(None)
    sorted_nssets = sorted(nssets.items(), key=lambda x: len(x[1]),  reverse=True)
    if args.output is None:
        write_output(sorted_nssets, sys.stdout)
    else:
        with open(args.output, "w") as out:
            write_output(sorted_nssets, out)


def parseargs():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("queries", help="file with inspected queries")
    argparser.add_argument("-d", "--data", default="queries",
                           help="directory with pickled query data (default 'queries')")
    argparser.add_argument("-o", "--output",
                           help="file to store output CSV (prints to stdout if not specified)")
    argparser.add_argument("-a", "--all", action="store_true",
                           help="aggregate by ALL name servers used during resolving query")
    argparser.add_argument("-p", "--path", action="store_true",
                           help="aggregate by all name servers of zones in path from root to \
                           queried name")
    args = argparser.parse_args()
    return args


if __name__ == '__main__':
    arguments = parseargs()
    create_report(arguments)
