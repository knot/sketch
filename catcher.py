#!/usr/bin/python3
import sys
from typing import Optional, Dict, Tuple, Set, NamedTuple, Union

import pickle
import multiprocessing as mp
import os
import time

import dns
import dns.message
import dns.name
import dns.query
import dns.rdatatype
import dns.rcode


Name_t = dns.name.Name


class Query(NamedTuple):
    name: Name_t
    rdtype: int


class NoAnswer(NamedTuple):
    udp_err: Exception
    tcp_err: Exception


class Stats:
    def __init__(self):
        self.queries: int = 0
        self.cache: int = 0
        self.internet: int = 0
        self.udp_timeouts: int = 0
        self.tcp_timeouts: int = 0
        self.max_queries: int = 0
        self.timeout_recover: int = 0
        self.time = None

    def print_stats(self):
        print("queries:", self.queries)
        print("from internet:", self.internet)
        print("from cache:", self.cache)
        print("udp timeouts:", self.udp_timeouts)
        print("tcp timeouts:", self.tcp_timeouts)
        print("recovered timeouts:", self.timeout_recover)
        print("maximum of queries ready to resolve:", self.max_queries)
        print("resolver time:", self.time)


class Endpoint:
    def __init__(self, address: str):
        self.address = address
        self.protocol: Optional[str] = None

    def __eq__(self, other):
        return self.address == other.address

    def __hash__(self):
        return hash(self.address)


class QueryData:
    def __init__(self,
                 queries: Dict[Name_t, Set[Query]],
                 nssets: Dict[Name_t, Set[Name_t]],
                 addresses: Dict[Name_t, Set[str]],
                 cnames: Dict[Name_t, Set[Name_t]],
                 no_zonecuts: Set[Name_t],
                 answers: Dict[Tuple[Query, Endpoint], Union[dns.message.Message, NoAnswer]],
                 stats: Stats,
                 original_query: Query):
        self.stats = stats
        self.answers = answers
        self.no_zonecuts = no_zonecuts
        self.cnames = cnames
        self.addresses = addresses
        self.queries = queries
        self.nssets = nssets
        self.original_query = original_query

    def get_answers(self, query: Query, zone: dns.name.Name) -> Tuple[dns.rrset.RRset, bool]:
        """
        Get RRset with union of answer rdata from all servers and check their consistency
        """
        consistent = True
        rdatas = dns.rrset.RRset(query.name, dns.rdataclass.IN, query.rdtype)
        for address in self.get_ns_addresses(zone):
            answer = self.answers[query, Endpoint(address)]
            if isinstance(answer, NoAnswer):
                continue
            for rrset in answer.answer + answer.authority:
                if rrset.name == query.name and rrset.rdtype == query.rdtype:
                    if rdatas != rrset:
                        if rdatas:
                            consistent = False
                        rdatas.union_update(rrset)
        return rdatas, consistent

    def get_ns_addresses(self, zone: dns.name.Name):
        """
        get addresses of servers authoritative for zone
        """
        for ns in self.nssets.get(zone, []):
            for address in self.addresses[ns]:
                yield address

    def is_answered(self, query, zone_name):
        """
        Checks if there are servers with known address authoritative for the zone
        and if any of them has answered the question.
        """
        if zone_name not in self.nssets:
            return False
        for ip in self.get_ns_addresses(zone_name):
            key = (query, Endpoint(ip))
            if key not in self.answers:
                continue
            message = self.answers[key]
            if isinstance(message, NoAnswer):
                continue
            if message_question_answered(message):
                return True
            if message.flags & dns.flags.AA == 0:
                return True
        return False

    def save_file(self, directory: str):
        path = get_filename(self.original_query, directory)
        # write to a temporary file as guard against KeyboardInterrupt
        tmppath = '{}.tmp'.format(path)
        with open(tmppath, "wb") as f:
            pickle.dump(self, f)
        os.rename(tmppath, path)


class Environment:
    def __init__(self, cache: str, hint_file: str, verbose: bool, num_subprocesses=100):
        mp.set_start_method("forkserver")

        self.hints = hint_file  # file with root hints
        self.verbose = verbose

        self.resolving: Set[Tuple[Query, Endpoint]] = set()  # queries that are processed now
        self.waiting_queries = mp.Queue()  # queries ready to be answered
        self.waiting_answers = mp.Queue()  # answered queries waiting to be worked into the database
        self.workers = mp.Pool(num_subprocesses, worker,
                               (self.waiting_queries, self.waiting_answers, self.verbose))

        self.queries: Dict[Name_t, Set[Query]] = {}
        # zone name -> queries that servers the zone should be able to answer
        self.nssets: Dict[Name_t, Set[Name_t]] = {}
        # zone name -> domain names of nameservers associated with the zone
        self.addresses: Dict[Name_t, Set[str]] = {}  # domain name -> IP
        self.cnames: Dict[Name_t, Set[Name_t]] = {}  # alias -> canonical
        self.no_zonecuts: Set[Name_t] = set()
        self.resolved: Dict[Tuple[Query, Endpoint], Union[dns.message.Message, NoAnswer]] = {}
        # (query, IP) -> answer
        self.stats = Stats()

        self.cache_name: str = cache
        self.cache: Dict[Tuple[Query, Endpoint], Union[dns.message.Message, NoAnswer]] = \
            load_cache(cache)

        self.set_root_hints()

    def set_root_hints(self):
        with open(self.hints) as hints:
            for line in hints:
                split_line = line.split()
                if len(split_line) != 3:
                    continue
                name = dns.name.from_text(split_line[0])
                rdtype = dns.rdatatype.from_text(split_line[1])
                if rdtype == dns.rdatatype.NS:
                    target = dns.name.from_text(split_line[2])
                    if name not in self.nssets:
                        self.nssets[name] = {target}
                    else:
                        self.nssets[name].add(target)
                elif rdtype in (dns.rdatatype.A, dns.rdatatype.AAAA):
                    address = split_line[2]
                    if name not in self.addresses:
                        self.addresses[name] = {address}
                    else:
                        self.addresses[name].add(address)

    def get_query_data(self, original_query) -> QueryData:
        return QueryData(self.queries,
                         self.nssets,
                         self.addresses,
                         self.cnames,
                         self.no_zonecuts,
                         self.resolved,
                         self.stats,
                         original_query,
                         )

    def clear(self):
        self.cache.update(self.resolved)

        self.queries = {}
        self.nssets = {}
        self.addresses = {}
        self.cnames = {}
        self.no_zonecuts = set()
        self.resolved = {}
        self.stats = Stats()

        self.set_root_hints()

    def destroy(self):
        self.save_cache()
        self.workers.terminate()

    def save_cache(self):
        save_cache(self.cache_name, self.cache)

    def add_query_to_zone(self, query: Query, zone: Name_t):
        if zone not in self.queries:
            self.queries[zone] = set()
        self.queries[zone].add(query)
        if zone in self.cnames:
            for cname in self.cnames[zone]:
                if query not in self.queries[cname]:
                    self.add_query_to_zone(query, cname)
        if zone in self.no_zonecuts:
            parent = dns.name.Name(zone[1:])
            if query not in self.queries[parent]:
                self.add_query_to_zone(query, parent)
        new_queries = []
        for server in self.nssets.get(zone, []):
            for address in self.addresses.get(server, []):
                new_queries.append((query, Endpoint(address)))
        for new_query in new_queries:
            self.add_to_queue(new_query)

    # query is ready to be resolved
    def add_to_queue(self, q: Tuple[Query, Endpoint], ignore_duplicity=False, ignore_cache=False):
        if (q not in self.resolving and q not in self.resolved) or ignore_duplicity:
            self.stats.queries += 1
            if q in self.cache and not ignore_cache:
                self.stats.cache += 1
                answer = self.cache[q]
                if self.verbose:
                    print("CACHE:", q[0], "@", q[1].address)
                self.resolved[q] = answer
                if isinstance(answer, dns.message.Message):
                    self.work_in_answer(answer)
            else:
                self.stats.internet += 1
                self.resolving.add(q)
                self.waiting_queries.put(q)

    # add a query and queries that it requires cause of query minimization to the database
    def add_query(self, query: Query):
        name, rdtype = query

        belongs_to = [name]
        if rdtype != dns.rdatatype.NS:
            self.add_query(Query(name, dns.rdatatype.NS))
        else:
            if name != dns.name.root:
                parent = dns.name.Name(name[1:])
                belongs_to.append(parent)
                self.add_query(Query(parent, dns.rdatatype.NS))

        for zone in belongs_to:
            self.add_query_to_zone(query, zone)

    # add new name server to nsset and inspect if any new queries can be answered now
    def add_ns(self, nsset: Name_t, server: Name_t):
        if nsset not in self.nssets:
            self.nssets[nsset] = set()
        if server in self.nssets[nsset]:
            return
        self.nssets[nsset].add(server)
        if server not in self.addresses:
            self.addresses[server] = set()

        new_queries = []
        for query in self.queries.get(nsset, set()):
            for address in self.addresses[server]:
                new_queries.append((query, Endpoint(address)))
        self.add_query(Query(server, dns.rdatatype.A))
        self.add_query(Query(server, dns.rdatatype.AAAA))
        for q in new_queries:
            self.add_to_queue(q)

    # add address of name server and inspect if any new queries can be answered now
    def add_address(self, name: Name_t, address: str):
        if name not in self.addresses:
            self.addresses[name] = set()
        if address in self.addresses[name]:
            return
        self.addresses[name].add(address)

        new_queries = []
        for zone, nsset in self.nssets.items():
            if name in nsset:
                for query in self.queries.get(zone, set()):
                    new_queries.append((query, Endpoint(address)))
        for q in new_queries:
            self.add_to_queue(q)

    def add_cname(self, name: dns.name.Name, target: dns.name.Name):
        if name in self.cnames and self.cnames[name] == target:
            return
        if name in self.cnames:
            self.cnames[name].add(target)
        else:
            self.cnames[name] = {target}
        if name in self.queries:
            new_queries = set()
            for query in self.queries[name]:
                new_queries.add((query, target))
            for query, parent in new_queries:
                self.add_query_to_zone(query, parent)

        new_queries = set()
        for queries in self.queries.values():
            for query in queries:
                if query.name == name:
                    new_queries.add(Query(target, query.rdtype))
        for query in new_queries:
            self.add_query(Query(target, query.rdtype))

    def add_no_zonecut(self, name: dns.name.Name):
        self.no_zonecuts.add(name)
        if name in self.queries:
            parent = dns.name.Name(name[1:])
            new_queries = set()
            for query in self.queries[name]:
                new_queries.add((query, parent))
            for query, parent in new_queries:
                self.add_query_to_zone(query, parent)

    # actualize database with new answer
    def work_in_answer(self, message: dns.message.Message):
        # detect empty nonterminal
        if not message_question_answered(message):
            question = message.question[0]
            if question.rdtype == dns.rdatatype.NS:
                self.add_no_zonecut(question.name)

        # look for records that should change database
        for rrset in message.answer + message.authority + message.additional:
            if rrset.rdtype == dns.rdatatype.NS:
                for rdata in rrset:
                    self.add_ns(rrset.name, rdata.target)
            if rrset.rdtype in (dns.rdatatype.A, dns.rdatatype.AAAA):
                for rdata in rrset:
                    self.add_address(rrset.name, rdata.address)
            if rrset.rdtype == dns.rdatatype.CNAME:
                for rdata in rrset:
                    self.add_cname(rrset.name, rdata.target)

    def resolve(self, main_query: Query) -> QueryData:
        start_time = time.time()
        self.add_query(main_query)
        while self.resolving:
            query, endpoint, answer = self.waiting_answers.get()
            if isinstance(answer, NoAnswer):
                self.stats.tcp_timeouts += 1
                self.stats.udp_timeouts += 1
            elif endpoint.protocol == "tcp":
                self.stats.udp_timeouts += 1
            if len(self.resolving) > self.stats.max_queries:
                self.stats.max_queries = len(self.resolving)
            self.resolving.discard((query, endpoint))
            self.resolved[(query, endpoint)] = answer
            if isinstance(answer, dns.message.Message):
                self.work_in_answer(answer)
        for q, answer in self.resolved.items():
            if isinstance(answer, NoAnswer):
                self.add_to_queue(q, ignore_duplicity=True)
        while self.resolving:
            query, endpoint, answer = self.waiting_answers.get()
            self.resolving.discard((query, endpoint))
            self.resolved[(query, endpoint)] = answer
            if isinstance(answer, dns.message.Message):
                self.stats.timeout_recover += 1
                self.work_in_answer(answer)
        self.stats.time = time.time() - start_time
        data = self.get_query_data(main_query)
        self.clear()
        return data


def worker(queries: mp.Queue, answers: mp.Queue, verbose):
    while True:
        query, endpoint = queries.get()
        answer = ask(query, endpoint, verbose)
        answers.put((query, endpoint, answer))


# get answer to a query form server
def ask(query: Query, endpoint: Endpoint, verbose: bool) -> Union[dns.message.Message, NoAnswer]:
    if verbose:
        print("INTERNET:", query, "@", endpoint.address)
    question = dns.message.make_query(*query)
    question.use_edns()
    try:
        answer = dns.query.udp(question, endpoint.address, timeout=2, ignore_trailing=True)
        endpoint.protocol = "udp_edns0"
        if answer.rcode() not in (dns.rcode.NOERROR, dns.rcode.NXDOMAIN):
            question.use_edns(edns=-1)
            answer = dns.query.udp(question, endpoint.address, timeout=2, ignore_trailing=True)
            endpoint.protocol = "udp_noedns"
    except Exception as error:
        print("udp:", query, endpoint.address, error, file=sys.stderr)
        try:
            answer = dns.query.tcp(question, endpoint.address, timeout=2)
            endpoint.protocol = "tcp"
        except Exception as error2:
            print("tcp:", query, endpoint.address, error2, file=sys.stderr)
            answer = NoAnswer(error, error2)
    return answer


# Return true if the question section of message is answered in answer or authority section
def message_question_answered(message: dns.message.Message) -> bool:
    question_name = message.question[0].name
    question_rdtype = message.question[0].rdtype
    for rrset in message.answer + message.authority:
        if rrset.name == question_name and rrset.rdtype in (question_rdtype, dns.rdatatype.CNAME):
            return True
    return False


def load_cache(path: str) -> Optional[Dict[Tuple[Query, Endpoint],
                                           Union[dns.message.Message, NoAnswer]]]:
    try:
        with open(path, "rb") as f:
            cache = pickle.load(f)
            return cache
    except FileNotFoundError:
        return {}


def save_cache(path: str, cache: Dict[Tuple[Query, Endpoint],
                                      Union[dns.message.Message, NoAnswer]]):
    # write to a temporary file as guard against KeyboardInterrupt
    tmppath = '{}.tmp'.format(path)
    with open(tmppath, "wb") as f:
        pickle.dump(cache, f)
    os.rename(tmppath, path)


def get_filename(query: Query, path: str) -> str:
    return f"{path}/{query.name.to_text()}_{dns.rdatatype.to_text(query.rdtype)}.pkl"
