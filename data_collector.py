#!/usr/bin/python3
import argparse
import sys
import os

import dns.name
import dns.rdatatype

from catcher import Environment, Query


def resolve_queries(args):
    env = Environment(args.cache, args.hints, args.verbose, num_subprocesses=args.processes)
    num = 1
    try:
        with open(args.input) as queries:
            for line in queries:
                if args.verbose:
                    print(f"guery {num}: {line[:-1]}")
                if not line:
                    continue
                spl = line.split()
                if len(spl) != 2:
                    print(f"invalid length of line '{line[:-1]}', skipping", file=sys.stderr)
                    continue
                name = dns.name.from_text(spl[0])
                try:
                    rdtype = dns.rdatatype.from_text(spl[1])
                except (dns.rdatatype.UnknownRdatatype, ValueError):
                    print(f"unknown rdtype '{spl[1]}', skipping", file=sys.stderr)
                    continue
                query = Query(name, rdtype)
                try:
                    database = env.resolve(query)
                    print("QUERY:", line)
                    database.stats.print_stats()
                    database.save_file(args.output)
                except Exception as e:
                    print(f"ERROR while resolving line {line[:-1]}:\n{e}", file=sys.stderr)
                env.clear()
    finally:
        env.destroy()


def positive_int(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError(f"{ivalue} is not a positive number")
    return ivalue


def parse_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("input", help="file with queries to resolve")
    argparser.add_argument("-c", "--cache", default="sketch-cache.pkl",
                           help="file to find/store cache (default sketch-cache.pkl)")
    argparser.add_argument("-o", "--output", default="queries",
                           help="directory with output files (default 'queries')")
    argparser.add_argument("-H", "--hints", default="root_hints.txt",
                           help="file with root hints (default root_hits.txt)")
    argparser.add_argument("-p", "--processes", default=100, type=positive_int,
                           help="number of subbprocesses (default 100)")
    argparser.add_argument("-v", "--verbose", action="store_true",
                           help="verbosed output")
    args = argparser.parse_args()
    return args


if __name__ == '__main__':
    arguments = parse_args()
    os.makedirs(arguments.output, exist_ok=True)
    resolve_queries(arguments)
