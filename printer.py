#!/usr/bin/python3

import argparse
import pickle
import dns.rdatatype

from catcher import QueryData, Query

COLUMN_WIDTH = 48


class Printer:
    def __init__(self, data: QueryData, color: bool):
        self.data = data
        self.color = color

    def print_subtree(self, name, rdtype, depth, zone: dns.name.Name,
                      prefix: str, last: bool = False):
        query_str = f"{prefix} {name} {dns.rdatatype.to_text(rdtype)}"
        num_spaces = COLUMN_WIDTH - len(query_str)
        if num_spaces <= 0:
            num_spaces = 1
        print(query_str, end=" " * num_spaces)
        answers, is_consistent = self.data.get_answers(Query(name, rdtype), zone)
        if answers and rdtype == dns.rdatatype.NS:
            child_answers, child_consistent = self.data.get_answers(Query(name, rdtype), name)
            if not child_consistent:
                is_consistent = False
            if child_answers != answers:
                is_consistent = False
                answers.union_update(child_answers)
        if not is_consistent:
            if self.color:
                print("\033[91m", end="")  # red text
            print("!", end=" ")
        else:
            print(" ", end=" ")
        answers_to_print = sorted([str(rdata) for rdata in answers])
        if answers_to_print:
            print(f"{answers_to_print}", end="")

        if name in self.data.cnames:
            print(f"CNAME to {self.data.cnames[name]}")
        else:
            print()
        if self.color:
            print("\033[0m", end="")  # normal color
        if rdtype == dns.rdatatype.NS:
            queries = sorted(self.data.queries[name])
            queries = [query for query in queries if (query.name != name or query.rdtype != rdtype)
                       and self.data.is_answered(query, name)]
            for i, query in enumerate(queries):
                new_last = (i == len(queries) - 1)
                if last:
                    new_prefix = prefix[:-1] + " "
                else:
                    new_prefix = prefix[:-1] + "│"

                if new_last:
                    new_prefix = new_prefix + "└"
                else:
                    new_prefix = new_prefix + "├"

                self.print_subtree(query.name, query.rdtype, depth + 1, name, new_prefix, new_last)


def parseargs():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("data",
                           help="path pickled domain data")
    argparser.add_argument("-c", "--color", action="store_true",
                           help="colored output")
    args = argparser.parse_args()
    return args


def main():
    args = parseargs()
    path = args.data
    try:
        with open(path, "rb") as f:
            data: QueryData = pickle.load(f)
    except FileNotFoundError as e:
        print(e)
        return
    printer = Printer(data, args.color)
    printer.print_subtree(dns.name.root, dns.rdatatype.NS, 0, dns.name.root, "", True)


if __name__ == '__main__':
    main()
